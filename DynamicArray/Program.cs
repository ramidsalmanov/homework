﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DynamicArray
{
    class Program
    {
        static DynamicArray<int> array;
        static void Main(string[] args)
        {
            var arr = Enumerable.Range(0, 10);
            array = new DynamicArray<int>(arr);
            Console.WriteLine("Создан массив:");
            Print();

            Console.WriteLine($"Добавление элемента 25 в конец массива");
            array.Add(25);
            Print();

            int start = 100;
            int count = 15;
            Console.WriteLine($"Добавление списка элементов в массив. Добавлено {count} элементов начиная с {start}");
            var arr2 = Enumerable.Range(start, count);
            array.AddRange(arr2);
            Print();

            int element = 777;
            int index = 10;
            Console.WriteLine($"Вставка элемента по индексу. По индексу {index} вставлен элемент {element}");
            array.Insert(element, index);
            Print();

            Console.ReadLine();
        }

        private static void Print()
        {
            Console.WriteLine($"Длинна массива {array.Length} элементов");
            foreach (var item in array)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine("\n" + new string('-', 25));
        }

    }
}
